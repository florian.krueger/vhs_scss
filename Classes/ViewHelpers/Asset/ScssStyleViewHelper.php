<?php

declare(strict_types=1);

namespace Casix\VhsScss\ViewHelpers\Asset;

/*
 * This file is part of the vhs_scss project under GPLv2 or later.
 *
 * For the full copyright and license information, please read the
 * LICENSE.md file that was distributed with this source code.
 */

use Casix\VhsScss\Compiler;
use FluidTYPO3\Vhs\ViewHelpers\Asset\StyleViewHelper;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class ScssStyleViewHelper extends StyleViewHelper
{
    public function initializeArguments(): void
    {
        parent::initializeArguments();
        $this->registerArgument('scssphpOutputStyle', 'string', 'scssphp output style (see scssphp documentation for possible values)');
        $this->registerArgument('scssVariables', 'mixed', 'An optional array of variables to be set inside the SCSS context', false);
        $this->registerArgument('generateSourceMap', 'boolean', 'Whether a source map should be generated');
    }

    public function build(): string
    {
        /** @var Compiler $compiler */
        $compiler = $this->objectManager->get(Compiler::class);
        $compiler->configureFromTypoScript();
        if (isset($this->arguments['scssVariables']) && is_array($this->arguments['scssVariables'])) {
            $compiler->setVariables($this->arguments['scssVariables']);
        }
        if (!empty($this->arguments['scssphpOutputStyle'] ?? '')) {
            $compiler->setOutputStyle($this->arguments['scssphpOutputStyle']);
        }
        if (isset($this->arguments['generateSourceMap'])) {
            $compiler->setGenerateSourceMap($this->arguments['generateSourceMap']);
        }
        $this->setSourceOnCompiler($compiler);
        return $compiler->compile();
    }

    protected function setSourceOnCompiler(Compiler $compiler): void
    {
        if (isset($this->arguments['external']) && (bool)$this->arguments['external']) {
            throw new \Exception('SCSS files cannot be included from external sources', 1518790785);
        } else if (!empty($this->arguments['path'] ?? '')) {
            $compiler->setScssFile(GeneralUtility::getFileAbsFileName($this->arguments['path']));
        } else {
            $compiler->setScss($this->getContent());
        }
    }
}

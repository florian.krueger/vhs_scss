<?php

declare(strict_types=1);

namespace Casix\VhsScss;

/*
 * This file is part of the vhs_scss project under GPLv2 or later.
 *
 * For the full copyright and license information, please read the
 * LICENSE.md file that was distributed with this source code.
 */

use ScssPhp\ScssPhp\Compiler as ScssPhpCompiler;
use ScssPhp\ScssPhp\ValueConverter;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Cache\Frontend\FrontendInterface;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

class Compiler
{
    /**
     * @var FrontendInterface
     */
    protected $cache;

    /**
     * @var ScssPhpCompiler
     */
    protected $compiler;

    /**
     * @var ConfigurationManagerInterface
     */
    protected $configurationManager;

    /**
     * @var string
     */
    protected $scss = '';

    /**
     * @var string|null
     */
    protected $path = null;

    /**
     * Additional settings to include in cache hash calculation
     *
     * @var array
     */
    protected $additionalCacheVariables = [];

    public function __construct(CacheManager $cacheManager, ConfigurationManagerInterface $configurationManager)
    {
        $this->cache = $cacheManager->getCache('vhs_scss');
        $this->compiler = new ScssPhpCompiler();
        $this->configurationManager = $configurationManager;
    }

    /**
     * Add to scss variables, overriding previous values for identical keys
     */
    public function setVariables(array $variables): void
    {
        foreach ($variables as $variableName => $value) {
            $this->compiler->addVariables([$variableName => ValueConverter::parseValue($value)]);
        }
    }

    public function setScssFile(string $scssFile): void
    {
        $realFile = realpath($scssFile);
        $this->scss = file_get_contents($realFile);
        $this->path = $realFile;
    }

    public function setScss(string $scss): void
    {
        $this->scss = $scss;
    }

    public function setOutputStyle(string $outputStyle): void
    {
        $this->compiler->setOutputStyle($outputStyle);
        $this->additionalCacheVariables['outputStyle'] = $outputStyle;
    }

    public function setGenerateSourceMap(bool $generateSourceMap): void
    {
        if ($generateSourceMap) {
            $this->compiler->setSourceMap(ScssPhpCompiler::SOURCE_MAP_INLINE);
            // It is difficult to locate the original SCSS files with vhs assets and they will probably
            // be in a Resources/Private/ folder that is not being served by the web server anyway,
            // so include sources in SCSS.
            $this->compiler->setSourceMapOptions(['outputSourceFiles' => true]);
        } else {
            $this->compiler->setSourceMap(ScssPhpCompiler::SOURCE_MAP_NONE);
        }
        $this->additionalCacheVariables['sourceMap'] = $generateSourceMap;
    }

    public function configure(array $settings): void
    {
        if (!empty($settings['outputStyle'] ?? '')) {
            $this->setOutputStyle($settings['outputStyle']);
        }
        if (isset($settings['variables']) && is_array($settings['variables'])) {
            $this->setVariables($settings['variables']);
        }
        if (isset($settings['generateSourceMap'])) {
            $this->setGenerateSourceMap((bool)$settings['generateSourceMap']);
        }
    }

    public function configureFromTypoScript(): void
    {
        $ts = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS, 'vhsscss');
        if (is_array($ts)) {
            $this->configure($ts);
        }
    }

    public function compile(): string
    {
        $cached = $this->fetchFromCache();
        if (NULL !== $cached) {
            return $cached;
        }

        $css = $this->compileScss();
        $this->insertIntoCache($css);
        return $css;
    }

    protected function areFilesUnmodified(array $fileMtimeMap): bool
    {
        foreach ($fileMtimeMap as $file => $lastMtime) {
            $stat = stat($file);
            if (false === $stat || $lastMtime !== $stat['mtime']) {
                return false;
            }
        }

        return true;
    }

    protected function insertIntoCache(string $css): void
    {
        $this->cache->set($this->cacheHash(), [
            'parsedFiles' => $this->compiler->getParsedFiles(),
            'css' => $css
        ]);
    }

    protected function fetchFromCache(): ?string
    {
        $cacheEntry = $this->cache->get($this->cacheHash());
        if (false !== $cacheEntry) {
            $parsedFiles = $cacheEntry['parsedFiles'];
            if ($this->areFilesUnmodified($parsedFiles)) {
                return $cacheEntry['css'];
            }
        }

        return NULL;
    }

    protected function compileScss(): string
    {
        return $this->compiler->compile($this->scss, $this->path);
    }

    protected function cacheHash(): string
    {
        return sha1(implode('###', [$this->scss, $this->path || '', serialize($this->compiler->getVariables()), serialize($this->additionalCacheVariables)]));
    }
}
